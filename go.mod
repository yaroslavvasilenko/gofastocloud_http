module gitlab.com/fastogt/gofastocloud_http

go 1.16

require (
	github.com/gorilla/websocket v1.5.0
	github.com/stretchr/testify v1.8.0
	gitlab.com/fastogt/gofastocloud v1.8.7
	gitlab.com/fastogt/gofastocloud_base v1.6.2
	gitlab.com/fastogt/gofastogt v1.3.0
)
