package gofastocloud_http

import (
	"encoding/json"
	"net/url"

	"github.com/gorilla/websocket"
	"gitlab.com/fastogt/gofastocloud/media"
)

func (fasto *FastoCloud) Run() {
	u, err := url.Parse(fasto.generateRoute("/updates"))
	if err != nil {
		return
	}

	if u.Scheme == "https" {
		u.Scheme = "wss"
	} else {
		u.Scheme = "ws"
	}

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		return
	}

	defer c.Close()

	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			break
		}

		err = fasto.processUpdate(message)
		if err != nil {
			break
		}
	}
}

func (fasto *FastoCloud) processUpdate(rawMessage []byte) error {
	const kStatisticServiceBroadcastCommand = "statistic_service"
	const kQuitStatusStreamBroadcastCommand = "quit_status_stream"
	const kStatisticStreamBroadcastCommand = "statistic_stream"

	type WSMessage struct {
		Type string          `json:"type"`
		Data json.RawMessage `json:"data"`
	}

	var msg WSMessage
	err := json.Unmarshal(rawMessage, &msg)
	if err != nil {
		return err
	}

	if msg.Type == kStatisticServiceBroadcastCommand {
		var stat media.ServiceStatisticInfo
		err := json.Unmarshal(msg.Data, &stat)
		if err != nil {
			return err
		}
		fasto.statistics = &stat
	} else if msg.Type == kQuitStatusStreamBroadcastCommand {
		var quit media.QuitStatusInfo
		err := json.Unmarshal(msg.Data, &quit)
		if err != nil {
			return err
		}
	} else if msg.Type == kStatisticStreamBroadcastCommand {
		var stat media.StreamStatisticInfo
		err := json.Unmarshal(msg.Data, &stat)
		if err != nil {
			return err
		}
	}

	return nil
}
