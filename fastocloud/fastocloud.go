package gofastocloud_http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_http/fastocloud/public"
	"gitlab.com/fastogt/gofastogt"
)

type FastoCloud struct {
	Endpoint string
	Auth     *string
	Origin   string

	statistics *media.ServiceStatisticInfo
}

func NewFastoCloud(endpoint string, auth *string, origin string) *FastoCloud {
	return &FastoCloud{Endpoint: endpoint, Auth: auth, Origin: origin, statistics: nil}
}

func (fasto *FastoCloud) UploadFile(path string) (*public.UploadFile, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(file.Name()))
	if err != nil {
		return nil, err
	}

	io.Copy(part, file)
	writer.Close()
	return fasto.uploadFileFromReader(writer.FormDataContentType(), body)
}

//bucket
func (fasto *FastoCloud) MountBucket(name, path, key, secret string) (*public.Bucket, error) {
	var reqParams = public.MountBucketRequest{
		Name:   name,
		Path:   path,
		Key:    key,
		Secret: secret,
	}
	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("s3bucket/mount", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.BucketResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) UnmountBucket(path string) (*public.Bucket, error) {

	var reqParams = public.UnmountBucketRequest{
		Path: path,
	}
	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("s3bucket/unmount", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.BucketResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetMountedBuckets() (*public.GetBucket, error) {
	req, err := fasto.makeHttpGetRequest("/s3bucket/list")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.GetBucketResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

//probe
func (fasto *FastoCloud) ProbeIn(url media.InputUri) (*public.Probe, error) {
	var reqParams = public.ProbeRequestIn{
		Url: url,
	}

	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/probe_in", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.ProbeResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) ProbeOut(url media.OutputUri) (*public.Probe, error) {
	var reqParams = public.ProbeRequestOut{
		Url: url,
	}

	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/probe_out", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.ProbeResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

//stream
func (fasto *FastoCloud) StartStream(config []byte) (*public.StartStream, error) {
	req, err := fasto.makeHttpPostRequest("/stream/start", "application/json", bytes.NewBuffer(config))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.StartStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) StopStream(sid media.StreamId, force bool) (*public.StopStream, error) {
	var reqParams = public.StopStreamRequest{
		Id:    sid,
		Force: force,
	}

	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/stream/stop", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.StopStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) CleanStream(config []byte) (*public.CleanStream, error) {
	req, err := fasto.makeHttpPostRequest("/stream/clean", "application/json", bytes.NewBuffer(config))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.CleanStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) StreamConfig(sid media.StreamId) (*public.StreamConfig, error) {
	req, err := fasto.makeHttpGetRequest(fmt.Sprintf("/stream/config/%s", sid))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.StreamConfigResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// restart
func (fasto *FastoCloud) RestartStream(sid media.StreamId) (*public.RestartStream, error) {
	var reqParams = public.RestartStreamRequest{
		Id: sid,
	}

	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/stream/restart", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.RestartStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}
	return &response.Data, nil
}

// Change input source
func (fasto *FastoCloud) ChangeInputSource(sid media.StreamId, channelId int) (*public.ChangeInputStream, error) {
	var reqParams = public.ChangeInputStreamRequest{
		Sid:       sid,
		ChannelId: channelId,
	}

	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/stream/change_source", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.ChangeInputStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}
	return &response.Data, nil
}

// stats
func (fasto *FastoCloud) GetStats() (*public.FullStatService, error) {
	req, err := fasto.makeHttpGetRequest("/stats")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)

	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var stat public.StatServiceResponse
	err = decoder.Decode(&stat)
	if err != nil {
		return nil, err
	}
	return &stat.Data, nil
}

func (fasto *FastoCloud) GetStreamStats(sid media.StreamId) (*public.StreamStatistic, error) {
	req, err := fasto.makeHttpGetRequest(fmt.Sprintf("/stream/stats/%s", sid))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)

	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.StreamStatisticResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetStreamsStats() (*public.StreamsStatistic, error) {
	req, err := fasto.makeHttpGetRequest("/stream_stats")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.StreamsStatisticResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

//video
func (fasto *FastoCloud) RemoveVideo(path string) (*public.RemoveVideo, error) {
	var reqParams = public.RemoveVideoRequest{
		Path: path,
	}

	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/video/remove", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.RemoveVideoResponce
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

//sync
func (fasto *FastoCloud) SyncConfigs(config []json.RawMessage) (*public.SyncConfig, error) {
	var reqParams = public.SyncConfigRequest{
		Configs: config,
	}
	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/sync", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.SyncConfigResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

//configs
func (fasto *FastoCloud) GetConfigs() (*public.StreamConfigs, error) {
	req, err := fasto.makeHttpGetRequest("/streams_configs")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.StreamConfigsResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}
	return &response.Data, nil
}

//folder
func (fasto *FastoCloud) ScanFolder(directory string, extensions []string) (*public.ScanFolder, error) {
	var reqParams = public.ScanFolderRequest{
		Directory:  directory,
		Extensions: extensions,
	}
	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/folder/scan", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.ScanFolderResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// Master Source Stream
func (fasto *FastoCloud) InjectMasterSourceStream(sid media.StreamId, url media.InputUri) (*public.InjectMasterInput, error) {
	var reqParams = public.InjectMasterInputRequest{
		Sid: sid,
		Url: url,
	}
	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/stream/inject_master_source", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.InjectMasterInputResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) RemoveMasterSourceStream(sid media.StreamId, url media.InputUri) (*public.RemoveMasterInput, error) {
	var reqParams = public.RemoveMasterInputRequest{
		Sid: sid,
		Url: url,
	}
	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/stream/remove_master_source", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.RemoveMasterInputResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// private:

func (fasto *FastoCloud) generateRoute(path string) string {
	return fasto.Endpoint + path
}

func (fasto *FastoCloud) makeHttpPostRequest(route string, content_type string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest("POST", fasto.generateRoute(route), body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", content_type)
	if fasto.Auth != nil {
		splited := strings.Split(*fasto.Auth, ":")
		if len(splited) == 2 {
			req.SetBasicAuth(splited[0], splited[1])
		}
	}

	return req, nil
}

func (fasto *FastoCloud) makeHttpGetRequest(route string) (*http.Request, error) {
	req, err := http.NewRequest("GET", fasto.generateRoute(route), nil)
	if err != nil {
		return nil, err
	}

	if fasto.Auth != nil {
		splited := strings.Split(*fasto.Auth, ":")
		if len(splited) == 2 {
			req.SetBasicAuth(splited[0], splited[1])
		}
	}

	return req, nil
}

func (fasto *FastoCloud) uploadFileFromReader(contentType string, reader io.Reader) (*public.UploadFile, error) {
	req, err := fasto.makeHttpPostRequest("/video/upload", contentType, reader)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if resp.StatusCode != http.StatusOK {
		var eresponse gofastogt.ErrorResponse
		err = decoder.Decode(&eresponse)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("wrong response status: %d, error: %v", resp.StatusCode, eresponse.Error)
	}

	var response public.UploadFileResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func checkErrorResponse(decoder *json.Decoder, response *http.Response) error {
	if response.StatusCode != http.StatusOK {
		var eresponse gofastogt.ErrorResponse
		err := decoder.Decode(&eresponse)
		if err != nil {
			return err
		}
		return fmt.Errorf("wrong response status: %d, error: %v", response.StatusCode, eresponse.Error)
	}
	return nil
}
